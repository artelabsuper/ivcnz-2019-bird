# Are These Birds Similar: Learning Branched Networks for Fine-grained Representations
This repository contain the code of the better experiments done to the CUB-200 2011 dataset.
Our model is able to combine the results of NTS-Net and Bert network to obtain better results.

![Model graph](imgs/model.png)
## Usage
The data are preprocessed with NTS-Net and Bert, 
after it is stored into comb-test.txt and comb-train.txt.

Install requirements:
```bash
pip install -r requirements.txt
```
Run the code:
```bash
python mlp-seperate_one_hidden_layer.py
```
## Dataset
The used dataset is the [Cub-200 2011](http://www.vision.caltech.edu/visipedia/CUB-200-2011.html).
Over there an example of dataset structure:

![Dataset example](imgs/dataset_sample.png)

## Result
| Method | Accuracy(%) |
| ---      |  ------  |
| Visual Stream (NTS-Net)[2]   | 87.50 | 
| Text Stream (Bert)[3]  | 65.00 | 
| Proposed Model [1]   | 96.81 | 

## Citation
```
@INPROCEEDINGS{Gallo:2019:IVCNZ, 
  author={Nawaz, Shah and Calefati, Alessandro and Caraffini, Moreno and Landro, Nicola and Gallo, Ignazio},
  booktitle={2019 International Conference on Image and Vision Computing New Zealand (IVCNZ 2019)}, 
  title={Are These Birds Similar: Learning Branched Networks for Fine-grained Representations},
  year={2019}, 
  month={Dec},
}
```

## References
[1] [Proposed Model](http://artelab.dista.uninsubria.it/res/research/papers/2019/2019-IVCNZ-Nawaz-Birds.pdf)

[2] NTS-Net: Z.  Yang,  T.  Luo,  D.  Wang,  Z.  Hu,  J.  Gao,  and  L.  Wang,  “Learning  tonavigate for fine-grained classification,” inProceedings of the EuropeanConference on Computer Vision (ECCV), 2018, pp. 420–435

[3] Bert: C.  Alberti,  K.  Lee,  and  M.  Collins,  “A  bert  baseline  for  the  naturalquestions,”arXiv preprint arXiv:1901.08634, 2019.
