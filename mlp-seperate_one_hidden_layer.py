import csv
import os

import numpy as np
import tensorflow as tf

os.environ['CUDA_VISIBLE_DEVICES'] = '1'

# Network Parameters
n_hidden_1 = 1000  # 1st layer number of neurons
n_input = 200  # data input
n_classes = 200  # total classes

# Store layers weight & bias
weights = {
    'h1': tf.Variable(tf.random_normal([n_input, n_hidden_1])),
    'out': tf.Variable(tf.random_normal([n_hidden_1, n_classes]))
}
biases = {
    'b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'out': tf.Variable(tf.random_normal([n_classes]))
}

with tf.name_scope('input'):
    input_images = tf.placeholder(tf.float32, shape=(None, n_input), name='input_images')
    input_txt = tf.placeholder(tf.float32, shape=(None, n_input), name='txt_images')
    labels = tf.placeholder(tf.int64, shape=(None), name='labels')

global_step = tf.Variable(0, trainable=False, name='global_step')


def build_network(input_images, input_txt, labels, weigths, biases, num_classes):
    logits_v = multilayer_perceptron(input_images, weigths, biases)
    logits_t = multilayer_perceptron(input_txt, weigths, biases)

    # logits=  tf.concat([logits_v,logits_t], 1)

    logits = tf.add(logits_v, logits_t)

    with tf.variable_scope('loss') as scope:
        with tf.name_scope('git_loss'):
            softmax_loss_v = tf.reduce_mean(
                tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=labels))
        scope.reuse_variables()
        # with tf.name_scope('softmax_loss'):
        #     softmax_loss_t = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits_t, labels=labels))
        with tf.name_scope('total_loss'):
            total_loss = softmax_loss_v  # + softmax_loss_t  #+  c_loss

    with tf.name_scope('acc'):
        accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(logits, 1), labels), tf.float32))

    with tf.name_scope('loss/'):
        # tf.summary.scalar('CenterLoss', softmax_loss_t)
        tf.summary.scalar('SoftmaxLoss', softmax_loss_v)
        tf.summary.scalar('TotalLoss', total_loss)

    return logits_v, total_loss, accuracy  # returns total loss


def multilayer_perceptron(x, weights, biases):
    # Hidden fully connected layer with 256 neurons
    layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
    # Output fully connected layer with a neuron for each class
    out_layer = tf.matmul(layer_1, weights['out']) + biases['out']
    return out_layer


def center_loss(features, labels, alfa, num_classes):
    nrof_features = features.get_shape()[1]
    centers = tf.get_variable('centers', [num_classes, nrof_features], dtype=tf.float32,
                              initializer=tf.constant_initializer(0), trainable=False)
    label = tf.reshape(labels, [-1])
    centers_batch = tf.gather(centers, label)
    diff = (1 - alfa) * (centers_batch - features)
    centers = tf.scatter_sub(centers, label, diff)
    loss = tf.reduce_mean(tf.square(features - centers_batch))
    return loss, centers


def read_file(file_name):
    img_lst = []
    txt_lst = []
    feat_lst = []
    label_lst = []
    with open(file_name) as fr:
        reader = csv.reader(fr, delimiter=',')
        for row in reader:
            class_label = int(row[-1])
            row = row[:-1]
            s_feat = [float(i) for i in row]
            s_feat = s_feat
            img = s_feat[0:200]
            txt = s_feat[200:]

            img_lst.append(img)
            txt_lst.append(txt)
            feat_lst.append(s_feat)
            label_lst.append(class_label)
    return feat_lst, img_lst, txt_lst, label_lst


def get_batch(batch_index, batch_size, labels, f_lst):
    start_ind = batch_index * batch_size
    end_ind = (batch_index + 1) * batch_size
    return np.asarray(f_lst[start_ind:end_ind]), np.asarray(labels[start_ind:end_ind])


features, total_loss, accuracy = build_network(input_images, input_txt, labels, weights,
                                               biases, n_classes)

train_file = 'comb-train.txt'
test_file = 'comb-test.txt'

train_set, img_train, txt_train, train_label = read_file(train_file)
test_set, img_test, txt_test, test_label = read_file(test_file)

print("Train file length", len(train_set))
print("Train file label length", len(train_label))
print("Test file length", len(test_set))
print("Test label length", len(test_label))

# combined = list(zip(train_set, train_label))
# random.shuffle(combined)
# train_set[:], train_label[:] = zip(*combined)

# combined = list(zip(img_train, train_label))
# random.shuffle(combined)
# img_train[:], train_label[:] = zip(*combined)
#
#
# combined = list(zip(txt_train, train_label))
# random.shuffle(combined)
# txt_train[:], train_label[:] = zip(*combined)


mean_data = np.mean(train_set, axis=0)

mean_data_img_train = np.mean(img_train, axis=0)
mean_data_img_test = np.mean(img_test, axis=0)

mean_data_txt_train = np.mean(txt_train, axis=0)
mean_data_txt_test = np.mean(txt_test, axis=0)

batch_size = 16
max_num_epoch = 40
steps_per_epoch = len(train_set) // batch_size
num_steps = steps_per_epoch * max_num_epoch

optimizer = tf.train.AdamOptimizer(0.001)  # learning rate.
train_op = optimizer.minimize(total_loss, global_step=global_step)

summary_op = tf.summary.merge_all()
sess = tf.Session()
sess.run(tf.global_variables_initializer())
saver = tf.train.Saver()
writer = tf.summary.FileWriter('maxi-fig', sess.graph)
step = sess.run(global_step) + 1

num_train_samples = len(train_set)
num_test_samples = len(test_set)
num_of_batches = num_train_samples // batch_size
num_of_batches_test = num_test_samples // batch_size

epochs = 200

for epoch in range(epochs):
    test_acc = 0.
    train_acc = 0.
    train_loss = 0.
    test_loss = 0.
    for idx in range(num_of_batches):
        batch_images, batch_labels = get_batch(idx, batch_size, train_label, img_train)
        batch_txt, _ = get_batch(idx, batch_size, train_label, txt_train)
        _, summary_str, train_batch_acc, train_batch_loss = sess.run(
            [train_op, summary_op, accuracy, total_loss],
            feed_dict={
                input_images: batch_images - mean_data_img_train,
                input_txt: batch_txt - mean_data_txt_train,
                labels: batch_labels,
            })
        writer.add_summary(summary_str, global_step=step)
        train_acc += train_batch_acc
        train_loss += train_batch_loss

    train_acc /= num_of_batches
    train_acc = train_acc * 100

    for s_batch in range(num_of_batches_test):
        batch_images_test, batch_labels_test = get_batch(s_batch, batch_size, test_label, img_test)
        batch_txt_test, _ = get_batch(s_batch, batch_size, test_label, txt_test)
        _, summary_str, test_batch_acc, test_loss_batch = sess.run(
            [train_op, summary_op, accuracy, total_loss],
            feed_dict={
                input_images: batch_images_test,
                input_txt: batch_txt_test,
                labels: batch_labels_test,
            })
        test_acc += test_batch_acc
        test_loss += test_loss_batch

    test_acc /= num_of_batches_test
    test_acc = test_acc * 100

    print(("Epoch: {}, Train_Acc:{:.4f}, Train_Loss:{:.4f}, Test_Acc:{:.4f}, Test_loss:{:.4f}".
           format(epoch, train_acc, train_loss, test_acc, test_loss)))
